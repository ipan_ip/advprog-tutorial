package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    protected FlyBehavior flyBehavior;
    protected QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    // TODO Complete me!
    public void setFlyBehavior(FlyBehavior fh) {
        flyBehavior = fh;
    }

    public void setQuackBehavior(QuackBehavior qh) {
        quackBehavior = qh;
    }

    public void swim() {
        System.out.println("All duck float, even decoys!");
    }

    public abstract void display();
}
